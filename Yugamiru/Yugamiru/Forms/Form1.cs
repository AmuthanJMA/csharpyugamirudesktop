﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Yugamiru
{
    public partial class Form1 : Yugamiru.Forms.BaseForm
    {
        public string test;
        public string status;
        public string dragiconstatus;
        public Form1()
        {

            test = "some test";
            status = "side";
            dragiconstatus = "belt";
            InitializeComponent();

            comboBox2.SelectedIndex = 0;

            mainpage.Image = Yugamiru.Properties.Resources.Mainpic;
          startinitial.Image = Yugamiru.Properties.Resources.startgreen_on;
            openresultbutton.Image = Yugamiru.Properties.Resources.dataload_on;
            quitapp.Image = Yugamiru.Properties.Resources.end_on;
            returnbutton.Image = Yugamiru.Properties.Resources.gobackgreen_down;
            nextbutton.Image = Yugamiru.Properties.Resources.gonextgreen_down;
            //sideimage.Image = Yugamiru.Properties.Resources.Mainpic3;
           // returnsideimage.Image = Yugamiru.Properties.Resources.gobackgreen_down;
           // opensideimage.Image = Yugamiru.Properties.Resources.imageload_down;
            //rotatesideimage.Image = Yugamiru.Properties.Resources.imagerotation_down;
            //Entersideimage.Image = Yugamiru.Properties.Resources.imagecopy_down;
           // nextsideimage.Image = Yugamiru.Properties.Resources.gonextgreen_down;
            Loadimage.Image = Yugamiru.Properties.Resources.Mainpic3;
            returnimage.Image = Yugamiru.Properties.Resources.gobackgreen_down;
            openimage.Image = Yugamiru.Properties.Resources.imageload_down;
            rotateimage.Image = Yugamiru.Properties.Resources.imagerotation_down;
            nextimage.Image = Yugamiru.Properties.Resources.gonextgreen_down;
            enteruprightimage.Image = Yugamiru.Properties.Resources.imagecopy_down;
            Speechimagetext.Image = Yugamiru.Properties.Resources.SpeechFrontStanding;
            //ouchedimage.Image = Yugamiru.Properties.Resources.Mainpic3;
            //nextcrouched.Image = Yugamiru.Properties.Resources.gonextgreen_down;
            //returncrouched.Image = Yugamiru.Properties.Resources.gobackgreen_down;
            //opencrouched.Image = Yugamiru.Properties.Resources.imageload_down;
            //rotatecrouched.Image = Yugamiru.Properties.Resources.imagerotation_down;
            //entercrouchedimage.Image = Yugamiru.Properties.Resources.imagecopy_down;
            //crouchedimagetext.Image = Yugamiru.Properties.Resources.SpeechFrontKneedown;
            beltanklescreen.Image = Yugamiru.Properties.Resources.Mainpic4;
            returndragiconscreen.Image = Yugamiru.Properties.Resources.gobackgreen_down;
            nextdragiconscreen.Image = Yugamiru.Properties.Resources.gonextgreen_on;
           // kneescreen.Image = Yugamiru.Properties.Resources.Mainpic4;
            //returnknee.Image = Yugamiru.Properties.Resources.gobackgreen_down;
            //nextscreen.Image = Yugamiru.Properties.Resources.gonextgreen_on;
           // earshoulderscreen.Image = Yugamiru.Properties.Resources.Mainpic4;
           // returnearshoulder.Image = Yugamiru.Properties.Resources.gobackgreen_down;
           // nextearshoulder.Image = Yugamiru.Properties.Resources.gonextgreen_on;
            sidescreen.Image = Yugamiru.Properties.Resources.Mainpic5;
            returnsideicon.Image = Yugamiru.Properties.Resources.gobackgreen_down;
            nextsideicon.Image = Yugamiru.Properties.Resources.completered_down;
            finalscreen.Image = Yugamiru.Properties.Resources.Mainpic6;
            editid.Image = Yugamiru.Properties.Resources.namechange_down;
            changebutton.Image = Yugamiru.Properties.Resources.imagechange_down;
            checkpositionbutton.Image = Yugamiru.Properties.Resources.jointedit_down;
            showreportbutton.Image = Yugamiru.Properties.Resources.reportdisplay_down;
            printreportbutton.Image = Yugamiru.Properties.Resources.reportprint_down;
            saveresultbutton.Image = Yugamiru.Properties.Resources.datasave_down;
            restartbutton.Image = Yugamiru.Properties.Resources.startred_down;
            initialscreenbutton.Image = Yugamiru.Properties.Resources.returnstart_down;

            minimumbeltankles.Image = Yugamiru.Properties.Resources.mag1_down;
            maximumbeltankles.Image = Yugamiru.Properties.Resources.mag2_down;
            originalsizebeltankles.Image = Yugamiru.Properties.Resources.mag3_down;

           // minimumknees.Image = Yugamiru.Properties.Resources.mag1_down;
           // maximumkness.Image = Yugamiru.Properties.Resources.mag2_down;
           // originalsizeknees.Image = Yugamiru.Properties.Resources.mag3_down;

          //  minimizeshoulderear.Image = Yugamiru.Properties.Resources.mag1_down;
          //  maximizeshouldersears.Image = Yugamiru.Properties.Resources.mag2_down;
          //  originalsizeshoulderear.Image = Yugamiru.Properties.Resources.mag3_down;

            minimizesideview.Image = Yugamiru.Properties.Resources.mag1_down;
            maximizesideview.Image = Yugamiru.Properties.Resources.mag2_down;
            originalsizesideview.Image = Yugamiru.Properties.Resources.mag3_down;

            minimizefinalscreen.Image = Yugamiru.Properties.Resources.mag1_down;
            maximizefinalscreen.Image = Yugamiru.Properties.Resources.mag2_down;
            originalsizefinalscreen.Image = Yugamiru.Properties.Resources.mag3_down;

            labelforiconsscreen.Text = "Drag icons to belt, ankles.";
            labelforsideicon.Text = "Drag each icons in side view.";

            Gender.SelectedIndex = 0;

        }

        private void pictureBox2_MouseDown(object sender, MouseEventArgs e)
        {
            startbutton.Image = Yugamiru.Properties.Resources.startgreen_down;
        }

        private void pictureBox2_MouseUp(object sender, MouseEventArgs e)
        {
            startbutton.Image = Yugamiru.Properties.Resources.startgreen_up;
        }

      

        private void quit_click(object sender, EventArgs e)
        {
            this.Close();

        }

        
        private void start_click(object sender, EventArgs e)
        {
            
            tabControl1.SelectedIndex = 1;
            startinitialscreen.Image = Yugamiru.Properties.Resources.Mainpic2;

            
        }

        private void pictureBox4_MouseDown(object sender, MouseEventArgs e)
        {
            quitbutton.Image = Yugamiru.Properties.Resources.end_down;
        }

        private void returnbutton_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 0;
        }

        private void nextbutton_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 3;
            useridlabel.Text = user_ID.Text;
            usernamelabel.Text = user_Name.Text;

               
        }

       

        private void button1_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 2;
        }

        

        private void button2_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 0;

        }

        private void returnfromsideimage_click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 1;
        }

        private void enterinside_click(object sender, EventArgs e)
        {
            
        }

        private void nextfromside_click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 4;
        }

        private void enterfromupright_click(object sender, EventArgs e)
        {
            nextimage.Visible = true;
        }

        private void nextimage_click(object sender, EventArgs e)
        {
            //tabControl1.SelectedIndex = 5;
            switch (status)
            {
                case "side":
                    //Loadimage.Image = Yugamiru.Properties.Resources.Mainpic3;
                    Speechimagetext.Visible = true;
                    Speechimagetext.Image = Yugamiru.Properties.Resources.SpeechFrontStanding;
                    nextimage.Visible = false;
                    status = "upright";
                    break;
                case "upright":
                    //Loadimage.Image = Yugamiru.Properties.Resources.Mainpic3;
                    Speechimagetext.Image = Yugamiru.Properties.Resources.SpeechFrontKneedown;
                    nextimage.Visible = false;
                    status = "crouched";
                    break;
                case "crouched":
                    tabControl1.SelectedIndex = 4;
                    break;
                

                default:
                    break;

        }
        }

        private void return_click(object sender, EventArgs e)
        {
            switch (status)
            {
                case "side":
                    tabControl1.SelectedIndex = 1;
                    break;
                case "upright":
                    Speechimagetext.Visible = false;
                    status = "side";
                    break;
                case "crouched":
                    status = "upright";
                    Speechimagetext.Image = Yugamiru.Properties.Resources.SpeechFrontStanding;


                    break;
                default:
                    break;
            }
            

        }

       

        private void returndragicon_click(object sender, EventArgs e)
        {
            switch (dragiconstatus)
                {
                case "belt":
                    tabControl1.SelectedIndex = 3;
                    break;
                case "knees":
                    dragiconstatus = "belt";
                    labelforiconsscreen.Text = "Drag icons to belt, ankles.";
                    break;
                case "shoulder":
                    dragiconstatus = "knees";
                    labelforiconsscreen.Text = "Drag icons to knees.";

                    break;
                default:
                    break;
            }
            
        }

        private void nextdragicon_Click(object sender, EventArgs e)
        {
            switch (dragiconstatus)
            {
                case "belt":
                    dragiconstatus = "knees";
                    labelforiconsscreen.Text = "Drag icons to knees.";
                    break;
                case "knees":
                    dragiconstatus = "shoulder";
                    labelforiconsscreen.Text = "Drag icons to shoulders, ears, etc.";
                    break;
                case "shoulder":
                    tabControl1.SelectedIndex = 5;
                    break;
                default:
                    break;
            }
                   
        }

       

        private void returnsideicon_click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 4;
        }

        private void nextsideicon_click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 6;
        }

        private void comboBox2_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            var t = comboBox2.SelectedItem ;
            if (comboBox2.SelectedItem.ToString() == "Japanese")
            {
                
                CultureInfo culture = CultureInfo.CreateSpecificCulture("ja-JP");
                CultureInfo.DefaultThreadCurrentUICulture = culture;
                ReloadControls();
                Refresh();
            }
            else if(comboBox2.SelectedItem.ToString() == "English")
            {
                CultureInfo culture = CultureInfo.CreateSpecificCulture("en");
                CultureInfo.DefaultThreadCurrentUICulture = culture;
                ReloadControls();
                Refresh();
            }
        }
      
        

        private void opensideimage_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void ReloadControls()
        {
            mainpage.Image = Yugamiru.Properties.Resources.Mainpic;
            startinitial.Image = Yugamiru.Properties.Resources.startgreen_on;
            openresultbutton.Image = Yugamiru.Properties.Resources.dataload_on;
            quitapp.Image = Yugamiru.Properties.Resources.end_on;
            returnbutton.Image = Yugamiru.Properties.Resources.gobackgreen_down;
            nextbutton.Image = Yugamiru.Properties.Resources.gonextgreen_down;
           // sideimage.Image = Yugamiru.Properties.Resources.Mainpic3;
           // returnsideimage.Image = Yugamiru.Properties.Resources.gobackgreen_down;
           // opensideimage.Image = Yugamiru.Properties.Resources.imageload_down;
           // rotatesideimage.Image = Yugamiru.Properties.Resources.imagerotation_down;
           // Entersideimage.Image = Yugamiru.Properties.Resources.imagecopy_down;
          //  nextsideimage.Image = Yugamiru.Properties.Resources.gonextgreen_down;
            Loadimage.Image = Yugamiru.Properties.Resources.Mainpic3;
            returnimage.Image = Yugamiru.Properties.Resources.gobackgreen_down;
            openimage.Image = Yugamiru.Properties.Resources.imageload_down;
            rotateimage.Image = Yugamiru.Properties.Resources.imagerotation_down;
            nextimage.Image = Yugamiru.Properties.Resources.gonextgreen_down;
            enteruprightimage.Image = Yugamiru.Properties.Resources.imagecopy_down;
            Speechimagetext.Image = Yugamiru.Properties.Resources.SpeechFrontStanding;
            //crouchedimage.Image = Yugamiru.Properties.Resources.Mainpic3;
            //nextcrouched.Image = Yugamiru.Properties.Resources.gonextgreen_down;
            //returncrouched.Image = Yugamiru.Properties.Resources.gobackgreen_down;
           // opencrouched.Image = Yugamiru.Properties.Resources.imageload_down;
           // rotatecrouched.Image = Yugamiru.Properties.Resources.imagerotation_down;
           // entercrouchedimage.Image = Yugamiru.Properties.Resources.imagecopy_down;
          //  crouchedimagetext.Image = Yugamiru.Properties.Resources.SpeechFrontKneedown;
            beltanklescreen.Image = Yugamiru.Properties.Resources.Mainpic4;
            returndragiconscreen.Image = Yugamiru.Properties.Resources.gobackgreen_down;
            nextdragiconscreen.Image = Yugamiru.Properties.Resources.gonextgreen_on;
           // kneescreen.Image = Yugamiru.Properties.Resources.Mainpic4;
           // returnknee.Image = Yugamiru.Properties.Resources.gobackgreen_down;
           // nextscreen.Image = Yugamiru.Properties.Resources.gonextgreen_on;
          //  earshoulderscreen.Image = Yugamiru.Properties.Resources.Mainpic4;
          //  returnearshoulder.Image = Yugamiru.Properties.Resources.gobackgreen_down;
          //  nextearshoulder.Image = Yugamiru.Properties.Resources.gonextgreen_on;
            sidescreen.Image = Yugamiru.Properties.Resources.Mainpic5;
            returnsideicon.Image = Yugamiru.Properties.Resources.gobackgreen_down;
            nextsideicon.Image = Yugamiru.Properties.Resources.completered_down;
            finalscreen.Image = Yugamiru.Properties.Resources.Mainpic6;
            editid.Image = Yugamiru.Properties.Resources.namechange_down;
            changebutton.Image = Yugamiru.Properties.Resources.imagechange_down;
            checkpositionbutton.Image = Yugamiru.Properties.Resources.jointedit_down;
            showreportbutton.Image = Yugamiru.Properties.Resources.reportdisplay_down;
            printreportbutton.Image = Yugamiru.Properties.Resources.reportprint_down;
            saveresultbutton.Image = Yugamiru.Properties.Resources.datasave_down;
            restartbutton.Image = Yugamiru.Properties.Resources.startred_down;
            initialscreenbutton.Image = Yugamiru.Properties.Resources.returnstart_down;

            minimumbeltankles.Image = Yugamiru.Properties.Resources.mag1_down;
            maximumbeltankles.Image = Yugamiru.Properties.Resources.mag2_down;
            originalsizebeltankles.Image = Yugamiru.Properties.Resources.mag3_down;

         

            minimizesideview.Image = Yugamiru.Properties.Resources.mag1_down;
            maximizesideview.Image = Yugamiru.Properties.Resources.mag2_down;
            originalsizesideview.Image = Yugamiru.Properties.Resources.mag3_down;

            minimizefinalscreen.Image = Yugamiru.Properties.Resources.mag1_down;
            maximizefinalscreen.Image = Yugamiru.Properties.Resources.mag2_down;
            originalsizefinalscreen.Image = Yugamiru.Properties.Resources.mag3_down;

        }

   
          private void startinitial_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 1;
            startinitialscreen.Image = Yugamiru.Properties.Resources.Mainpic2;
        }

        private void quitapp_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void settingsbutton_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 2;
           
        }
    }
}
