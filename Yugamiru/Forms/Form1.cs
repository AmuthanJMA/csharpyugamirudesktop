﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Emgu.CV;
using Emgu.Util;
using Emgu.CV.Structure;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;


namespace Yugamiru
{
    public partial class Form1 : Yugamiru.Forms.BaseForm
    {
        #region Global Variables

        Rectangle rect = new Rectangle(109, 0, 192, 241);
       
        bool isMouseDown = false;
        bool isrectmoved = false;
        Image<Bgr, Byte> originalImage;
        Image<Bgr, Byte> currentImage;
        Image<Bgr, Byte> resizedImage;
        Image<Bgr, Byte> flipimage = null;
        Image<Bgr, Byte> sideimage_first;
        Image<Bgr, Byte> uprightimage_first;
        Image<Bgr, Byte> crouchedimage_first;
        Image<Bgr, Byte> resizeduprightimage_first;
        Image<Bgr, Byte> resizedcrouchedimage_first;
        Image<Bgr, Byte> resizedsideimage_first;
        Rectangle circle1 = new Rectangle(150, 300, 10, 10);
        Rectangle circle2 = new Rectangle(250, 300, 10, 10);
        Rectangle rect1 = new Rectangle(100, 150, 5, 5);
        Rectangle rect2 = new Rectangle(300, 150, 5, 5);

        Rectangle circle3 = new Rectangle(150, 300, 10, 10);
        Rectangle circle4 = new Rectangle(250, 300, 10, 10);
        Rectangle rect3 = new Rectangle(100, 150, 5, 5);
        Rectangle rect4 = new Rectangle(300, 150, 5, 5);

        //circle for knees for imagebox1
        Rectangle circle5 = new Rectangle(196, 250, 8,8);
        Rectangle circle6 = new Rectangle(230, 250, 8,8);

        Rectangle circle7 = new Rectangle(189, 300, 8, 8);
        Rectangle circle8 = new Rectangle(239, 300, 8, 8);

        Rectangle circle9 = new Rectangle(189, 400, 8, 8);
        Rectangle circle10 = new Rectangle(239, 400, 8, 8);

        //circle for knees for imagebox2
        Rectangle circle11 = new Rectangle(196, 250, 8, 8);
        Rectangle circle12 = new Rectangle(230, 250, 8, 8);

        Rectangle circle13 = new Rectangle(189, 300, 8, 8);
        Rectangle circle14 = new Rectangle(239, 300, 8, 8);

        Rectangle circle15 = new Rectangle(189, 400, 8, 8);
        Rectangle circle16 = new Rectangle(239, 400, 8, 8);


        bool firstCircle = false;
        bool secondCircle = false;
        bool firstrect = false;
        bool secondrect = false;
        bool mouseCliked = false;
        bool kneesCircle = false;

        Bitmap Global_sideImage;//global variable to store processed side image
        Bitmap Global_uprightImage;//global variable to store processed upright image
        Bitmap Global_crouchedImage;//global variable to store processed crouched image

        Image<Bgr, Byte>[] imagePyramid_upright = new Image<Bgr, byte>[6];
        Image<Bgr, Byte>[] imagepyramid_crouched = new Image<Bgr, byte>[6];
        Image<Bgr, Byte>[] imagepyramid_side = new Image<Bgr, byte>[6];


        public string status;
        public string dragiconstatus;
        
        #endregion
        public Form1()
        {
            //Initialize all the controls
            InitializeComponent();

            //Reset all the controls with default values
            SetControls();
            
            // uploadsideview.Image = Yugamiru.Properties.Resources.sokui;
            float f1 = uploadsideview.Width / 2;
            float f2 = f1 - (rect.Width / 2);

            rect.X = (int)f2;


            sideimage_first = new Image<Bgr, Byte>(Yugamiru.Properties.Resources.sokui);     
            pictureBox1.Image = sideimage_first.Resize(372, 450, Emgu.CV.CvEnum.INTER.CV_INTER_CUBIC, false).ToBitmap();
            //originalImage = sideimage_first.Resize(372, 450, Emgu.CV.CvEnum.INTER.CV_INTER_CUBIC, false);
            sideimage_first = sideimage_first.Resize(192,241,Emgu.CV.CvEnum.INTER.CV_INTER_CUBIC,false);
            uploadsideview.Image = sideimage_first;
            resizedsideimage_first = sideimage_first;


        }

        private void pictureBox2_MouseDown(object sender, MouseEventArgs e)
        {
            disposeMemory(startinitial.Image);
            
            startinitial.Image = Yugamiru.Properties.Resources.startgreen_down;
            //startbutton.Image = Yugamiru.Properties.Resources.startgreen_down;
        }

        private void pictureBox2_MouseUp(object sender, MouseEventArgs e)
        {
            disposeMemory(startinitial.Image);
            startinitial.Image = Yugamiru.Properties.Resources.startgreen_up;

           // startbutton.Image = Yugamiru.Properties.Resources.startgreen_up;
        }



        private void quit_click(object sender, EventArgs e)
        {
            this.Close();

        }


     /*   private void start_click(object sender, EventArgs e)
        {

           
        }*/

        private void pictureBox4_MouseDown(object sender, MouseEventArgs e)
        {
            disposeMemory(quitbutton.Image);
            quitbutton.Image = Yugamiru.Properties.Resources.end_down;
            
        }

        private void returnbutton_Click(object sender, EventArgs e)
        {
            
            tabControl1.SelectedIndex = 0;

            // setting controls to null
            disposeMemory(startinitial.Image);
            disposeMemory(returnbutton.Image);
            disposeMemory(nextbutton.Image);

            mainpage.Image = Yugamiru.Properties.Resources.Mainpic;
            startinitial.Image = Yugamiru.Properties.Resources.startgreen_on;
            openresultbutton.Image = Yugamiru.Properties.Resources.dataload_up;
            quitapp.Image = Yugamiru.Properties.Resources.end_up;



        }

        private void nextbutton_Click(object sender, EventArgs e)
        {
            String gendervariable, yearvariable, monthvariable, dayvariable;
            tabControl1.SelectedIndex = 3;
            useridlabel.Text = user_ID.Text;
            usernamelabel.Text = user_Name.Text;

            gendervariable = Gender.GetItemText(Gender.SelectedItem);
            if (gendervariable == "MALE")
                genderlabel.Text = "M";
            else if (gendervariable == "FEMALE")
                genderlabel.Text = "F";
            else
                genderlabel.Text = "-";

            yearvariable = Year.Value.ToString();
            monthvariable = Month.GetItemText(Month.SelectedItem);
            dayvariable = Day.GetItemText(Day.SelectedItem);
            dateofbirthlabel.Text = dayvariable + "." + monthvariable + "." + yearvariable;

            heightlabel.Text = numericUpDown1.Value.ToString();


            // setting resources to picturebox
            Loadimage.Image = Yugamiru.Properties.Resources.Mainpic3;
            returnimage.Image = Yugamiru.Properties.Resources.gobackgreen_down;
            openimage.Image = Yugamiru.Properties.Resources.imageload_down;
            rotateimage.Image = Yugamiru.Properties.Resources.imagerotation_down;
            nextimage.Image = Yugamiru.Properties.Resources.gonextgreen_down;
            enteruprightimage.Image = Yugamiru.Properties.Resources.imagecopy_down;
            Speechimagetext.Image = Yugamiru.Properties.Resources.SpeechSideStanding;
        }



        private void button1_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 2;
        }



        private void button2_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 0;

        }

        private void returnfromsideimage_click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 1;
        }

        private void nextfromside_click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 4;

        }

        private void enterfromupright_click(object sender, EventArgs e)
        {
            nextimage.Visible = true;
            //rect.x = 88 because X value changes when sokui.bmp image loaded in default  
                if (resizedImage == null && isrectmoved == true)// checking whether we are using default image
                {

                    pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
                    sideimage_first = (Image<Bgr,Byte>)uploadsideview.Image;
                    sideimage_first.ROI = new Rectangle(rect.X - uploadsideview.Image.Size.Width / 2, rect.Y, uploadsideview.Image.Size.Width, rect.Height);
                    //pictureBox1.Image = sideimage_first.ToBitmap();
                    //sideimage_first.ToBitmap().SetResolution(currentImage.ToBitmap().HorizontalResolution, currentImage.ToBitmap().VerticalResolution);
                   // pictureBox1.Image = sideimage_first.Resize(372, 450, Emgu.CV.CvEnum.INTER.CV_INTER_CUBIC, false).ToBitmap();
                     pictureBox1.Image = originalImage.ToBitmap();
                }
                else if (resizedImage != null)
                {
                 pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
                if (isrectmoved == false)
                    pictureBox1.Image = currentImage.Resize(372, 450, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR, false).ToBitmap();
                else
                {
                    resizedImage.ROI = new Rectangle(rect.X - uploadsideview.Image.Size.Width / 2, rect.Y, uploadsideview.Image.Size.Width, rect.Height);
                    resizedImage.ToBitmap().SetResolution(currentImage.ToBitmap().HorizontalResolution,currentImage.ToBitmap().VerticalResolution);
                    pictureBox1.Image = resizedImage.ToBitmap();

                     //pictureBox1.Image = resizedImage.Resize(372, 450, Emgu.CV.CvEnum.INTER.CV_INTER_CUBIC, false).ToBitmap();
                }
            }

        }

        private void nextimage_click(object sender, EventArgs e)
        {
            
            switch (status)
            {
                case "side":
                    Speechimagetext.Visible = true;
                    disposeMemory(Speechimagetext.Image);
                    Speechimagetext.Image = Yugamiru.Properties.Resources.SpeechFrontStanding;
                    nextimage.Visible = false;
                    Global_sideImage = new Bitmap(pictureBox1.Image); // setting global side image to access in markers pages
                    resizedsideimage_first = (Image<Bgr,Byte>)uploadsideview.Image;
                   
                    

                   
                    //Setting default image
                    uprightimage_first = new Image<Bgr, Byte>(Yugamiru.Properties.Resources.ritui);
                    pictureBox1.Image = uprightimage_first.Resize(372, 450, Emgu.CV.CvEnum.INTER.CV_INTER_CUBIC, false).ToBitmap();

                    resizeduprightimage_first = uprightimage_first.Resize(192, 241, Emgu.CV.CvEnum.INTER.CV_INTER_CUBIC, false);
                    //resizedImage = sideimage_first;
                    uploadsideview.Image = resizeduprightimage_first;
                    sideimage_first = resizeduprightimage_first;
                    //rect.X = 88;//  to centre the rectangle while moving next page 

                    status = "upright"; //updating status for next page
                    break;

                case "upright":
                    disposeMemory(Speechimagetext.Image);
                    Speechimagetext.Image = Yugamiru.Properties.Resources.SpeechFrontKneedown;
                    nextimage.Visible = false;
                    Global_uprightImage = new Bitmap(pictureBox1.Image);// setting global upright image to access in markers pages
                    resizeduprightimage_first = (Image<Bgr, Byte>)uploadsideview.Image;

                    //Setting default image
                    crouchedimage_first = new Image<Bgr, Byte>(Yugamiru.Properties.Resources.kutui);
                    pictureBox1.Image = crouchedimage_first.Resize(372, 450, Emgu.CV.CvEnum.INTER.CV_INTER_CUBIC, false).ToBitmap();
                    originalImage = crouchedimage_first.Clone();
                    resizedcrouchedimage_first = crouchedimage_first.Resize(192, 241, Emgu.CV.CvEnum.INTER.CV_INTER_CUBIC, false);
                    //resizedImage = sideimage_first;
                    uploadsideview.Image = resizedcrouchedimage_first;
                    sideimage_first = resizedcrouchedimage_first;

                    // rect.X = 88;//  to centre the rectangle while moving next page 


                    status = "crouched";  //updating status for next page
                    break;

                case "crouched":
                    Global_crouchedImage = new Bitmap(pictureBox1.Image);
                    resizedcrouchedimage_first = (Image<Bgr, Byte>)uploadsideview.Image;

                    //imageBox1.Image = new Image<Bgr, Byte>(Global_uprightImage).Resize(430,472,Emgu.CV.CvEnum.INTER.CV_INTER_AREA);
                    Image<Bgr, byte> test = new Image<Bgr, Byte>(Global_uprightImage);

                    //Setting image pyramids
                    imagePyramid_upright[5] = test;
                    imagePyramid_upright[4] = test
                        .Resize(1.25, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);
                    imagePyramid_upright[3] = test
                        .Resize(1.50, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);
                    imagePyramid_upright[2] = test
                        .Resize(1.75, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);
                    imagePyramid_upright[1] = test
                        .Resize(2, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);
                    imagePyramid_upright[0] = test
                        .Resize(2.5, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);

                    imageBox1.Image = imagePyramid_upright[0];




                    //imageBox2.Image = new Image<Bgr, Byte>(Global_crouchedImage).Resize(430, 472, Emgu.CV.CvEnum.INTER.CV_INTER_AREA); 
                    Image<Bgr, byte> test1 = new Image<Bgr, Byte>(Global_crouchedImage);

                    //setting image pyramids
                    imagepyramid_crouched[5] = test1;
                    imagepyramid_crouched[4] = test1
                        .Resize(1.25, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);
                    imagepyramid_crouched[3] = test1
                        .Resize(1.50, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);
                    imagepyramid_crouched[2] = test1
                        .Resize(1.75, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);
                    imagepyramid_crouched[1] = test1
                        .Resize(2, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);
                    imagepyramid_crouched[0] = test1
                        .Resize(2.5, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);

                    imageBox2.Image = imagepyramid_crouched[0];

                    // setting global crouched image to accesss in markers pages
                    tabControl1.SelectedIndex = 4;

                    // Setting picturebox to images
                    
                    disposeMemory(beltanklescreen.Image);
                    disposeMemory(returndragiconscreen.Image);
                    disposeMemory(nextdragiconscreen.Image);
                    beltanklescreen.Image = Yugamiru.Properties.Resources.Mainpic4;
                    
                    returndragiconscreen.Image = Yugamiru.Properties.Resources.gobackgreen_down;
                    
                    nextdragiconscreen.Image = Yugamiru.Properties.Resources.gonextgreen_on;
                    break;
                default:
                    break;

            }
        }

        private void return_click(object sender, EventArgs e)
        {
            

            switch (status)
            {
                case "side":
                    tabControl1.SelectedIndex = 1;

                    // setting picturebox to null values
                    disposeMemory(Loadimage.Image);
                    disposeMemory(returnimage.Image);
                    disposeMemory(openimage.Image);
                    disposeMemory(rotateimage.Image);
                    disposeMemory(nextimage.Image);
                    disposeMemory(enteruprightimage.Image);
                    disposeMemory(Speechimagetext.Image);
                   

                    // setting controls to initial page

                    startinitialscreen.Image = Yugamiru.Properties.Resources.Mainpic2;
                    returnbutton.Image = Yugamiru.Properties.Resources.gobackgreen_down;
                    nextbutton.Image = Yugamiru.Properties.Resources.gonextgreen_down;

                    
                    break;
                case "upright":
                    Speechimagetext.Visible = false;
                    status = "side";
                    pictureBox1.Image = Global_sideImage;

                   /* sideimage_first = new Image<Bgr, Byte>(Yugamiru.Properties.Resources.ritui);
                    sideimage_first = sideimage_first.Resize(192, 241, Emgu.CV.CvEnum.INTER.CV_INTER_CUBIC, false);*/
                    uploadsideview.Image = resizedsideimage_first;
                    break;
                case "crouched":
                    status = "upright";
                    Speechimagetext.Image = Yugamiru.Properties.Resources.SpeechFrontStanding;
                    pictureBox1.Image = Global_uprightImage; //setting upright image
                    uploadsideview.Image = resizeduprightimage_first;

                    /*sideimage_first = new Image<Bgr, Byte>(Yugamiru.Properties.Resources.kutui);
                    sideimage_first = sideimage_first.Resize(192, 241, Emgu.CV.CvEnum.INTER.CV_INTER_CUBIC, false);*/
                   // uploadsideview.Image = sideimage_first;
                    break;
                default:
                    break;
            }
        }

        private void returndragicon_click(object sender, EventArgs e)
        {
            switch (dragiconstatus)
            {
                case "belt":
                    status = "crouched";
                    tabControl1.SelectedIndex = 3;

                    disposeMemory(Loadimage.Image);
                    disposeMemory(returnimage.Image);
                    disposeMemory(openimage.Image);
                    disposeMemory(rotateimage.Image);
                    disposeMemory(nextimage.Image);
                    disposeMemory(enteruprightimage.Image);
                    disposeMemory(Speechimagetext.Image);

                    // setting resources to picturebox
                    Loadimage.Image = Yugamiru.Properties.Resources.Mainpic3;
                    returnimage.Image = Yugamiru.Properties.Resources.gobackgreen_down;
                    openimage.Image = Yugamiru.Properties.Resources.imageload_down;
                    rotateimage.Image = Yugamiru.Properties.Resources.imagerotation_down;
                    nextimage.Image = Yugamiru.Properties.Resources.gonextgreen_down;
                    enteruprightimage.Image = Yugamiru.Properties.Resources.imagecopy_down;
                    Speechimagetext.Image = Yugamiru.Properties.Resources.SpeechFrontKneedown;

                    // Setting picturebox to images

                    beltanklescreen.Image = null;
                    returndragiconscreen.Image = null;
                    nextdragiconscreen.Image = null;

                    break;
                case "knees":
                    dragiconstatus = "belt";
                    labelforiconsscreen.Text = "Drag icons to belt, ankles.";
                    break;
                case "shoulder":
                    dragiconstatus = "knees";
                    labelforiconsscreen.Text = "Drag icons to knees.";
                    break;
                default:
                    break;
            }

        }

        private void nextdragicon_Click(object sender, EventArgs e)
        {
            
            switch (dragiconstatus)
            {
                case "belt":
                    dragiconstatus = "knees";
                    imageBox1.Invalidate();
                    imageBox1.Update();
                    imageBox2.Invalidate();
                    imageBox2.Update();
                    labelforiconsscreen.Text = "Drag icons to knees.";
                    break;
                case "knees":
                    dragiconstatus = "shoulder";
                    imageBox1.Invalidate();
                    imageBox1.Update();
                    imageBox2.Invalidate();
                    imageBox2.Update();
                    labelforiconsscreen.Text = "Drag icons to shoulders, ears, etc.";
                    break;
                case "shoulder":
                    tabControl1.SelectedIndex = 5;
                    //imageBox3.Image = new Image<Bgr, Byte>(Global_sideImage).Resize(419, 455, Emgu.CV.CvEnum.INTER.CV_INTER_AREA);
                    Image<Bgr, byte> test3 = new Image<Bgr, Byte>(Global_sideImage);

                    //Setting image pyramids
                    imagepyramid_side[5] = test3;
                    imagepyramid_side[4] = test3
                        .Resize(1.25, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);
                    imagepyramid_side[3] = test3
                        .Resize(1.50, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);
                    imagepyramid_side[2] = test3
                        .Resize(1.75, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);
                    imagepyramid_side[1] = test3
                        .Resize(2, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);
                    imagepyramid_side[0] = test3
                        .Resize(2.5, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);

                    imageBox3.Image = imagepyramid_side[0];

                    disposeMemory(sidescreen.Image);
                    disposeMemory(returnsideicon.Image);
                    disposeMemory(Finish.Image);
                

                    // setting picturebox with resources
                    sidescreen.Image = Yugamiru.Properties.Resources.Mainpic5;
                    returnsideicon.Image = Yugamiru.Properties.Resources.gobackgreen_down;
                    Finish.Image = Yugamiru.Properties.Resources.completered_down;

                    break;
                default:
                    break;
            }

        }



        private void returnsideicon_click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 4;

            // setting picturebox with null values
            disposeMemory(sidescreen.Image);
            disposeMemory(returnsideicon.Image);
            disposeMemory(Finish.Image);

            disposeMemory(beltanklescreen.Image);
            disposeMemory(returndragiconscreen.Image);
            disposeMemory(nextdragiconscreen.Image);
         

            beltanklescreen.Image = Yugamiru.Properties.Resources.Mainpic4;
            returndragiconscreen.Image = Yugamiru.Properties.Resources.gobackgreen_down;
            nextdragiconscreen.Image = Yugamiru.Properties.Resources.gonextgreen_on;

        }

        private void nextsideicon_click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 6;

            disposeMemory(finalscreen.Image);
            disposeMemory(editid.Image);
            disposeMemory(changebutton.Image);
            disposeMemory(checkpositionbutton.Image);
            disposeMemory(showreportbutton.Image);
            disposeMemory(printreportbutton.Image);
            disposeMemory(saveresultbutton.Image);
            disposeMemory(restartbutton.Image);
            disposeMemory(initialscreenbutton.Image);

            finalscreen.Image = Yugamiru.Properties.Resources.Mainpic6;
            editid.Image = Yugamiru.Properties.Resources.namechange_on;
            changebutton.Image = Yugamiru.Properties.Resources.imagechange_up;
            checkpositionbutton.Image = Yugamiru.Properties.Resources.jointedit_up;
            showreportbutton.Image = Yugamiru.Properties.Resources.reportdisplay_up;
            printreportbutton.Image = Yugamiru.Properties.Resources.reportprint_up;
            saveresultbutton.Image = Yugamiru.Properties.Resources.datasave_up;
            restartbutton.Image = Yugamiru.Properties.Resources.startred_up;
            initialscreenbutton.Image = Yugamiru.Properties.Resources.returnstart_up;

        }

        private void comboBox2_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            var selectedLanguage = comboBox2.SelectedItem.ToString();

            switch (selectedLanguage)
            {
                case "Japanese":
                    CultureInfo.DefaultThreadCurrentUICulture = new CultureInfo("ja-JP");
                    break;
                case "Thai":
                    CultureInfo.DefaultThreadCurrentUICulture = new CultureInfo("th-TH");
                    break;
                default:
                    CultureInfo.DefaultThreadCurrentUICulture = new CultureInfo("en");
                    break;
            }

            //Reload all the controls
            SetControls();
            //Finally refresh the parent form
            Refresh();
        }


        private void startinitial_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 1;

            disposeMemory(startinitialscreen.Image);
            disposeMemory(returnbutton.Image);
            disposeMemory(nextbutton.Image);
         

            // setting controls
            startinitialscreen.Image = Yugamiru.Properties.Resources.Mainpic2;
            returnbutton.Image = Yugamiru.Properties.Resources.gobackgreen_down;
            nextbutton.Image = Yugamiru.Properties.Resources.gonextgreen_down;


        }

        private void quitapp_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void settingsbutton_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 2;

        }

        private void initialscreenbutton_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 0;
        }
      
        private void openimage_Click(object sender, EventArgs e)
        {
            // open file dialog 
            OpenFileDialog open = new OpenFileDialog();
            open.InitialDirectory = "Desktop";
            open.FilterIndex = 2;
            open.RestoreDirectory = true;
            open.Title = "Open Image";
             


            // image filters
            open.Filter = "Image Files(*.jpg; *.jpeg; *.gif; *.bmp)|*.jpg; *.jpeg; *.gif; *.bmp";
            if (open.ShowDialog() == DialogResult.OK)
            {
                try
                {

                    //uploadimage = new Bitmap(open.FileName);
                    currentImage = new Image<Bgr,Byte>(open.FileName);
                   // originalImage = currentImage.Clone();

                    // to Resize image
                   // ImageResize(currentImage);

                    int oriWidth = currentImage.Size.Width;
                    int oriHeight = currentImage.Size.Height;

                    // To preserve the aspect ratio
                    float ratioX = (float)192 / (float)oriWidth;
                    float ratioY = (float)241 / (float)oriHeight;
                    float ratio = Math.Min(ratioX, ratioY);

                    // New width and height based on aspect ratio
                    int newWidth = (int)(oriWidth * ratio);
                    int newHeight = (int)(oriHeight * ratio);

                    // Convert other formats (including CMYK) to RGB.
                    Bitmap newImage = new Bitmap(newWidth, newHeight, PixelFormat.Format24bppRgb);
                    // Draws the image in the specified size with quality mode set to HighQuality
                    newImage.SetResolution(currentImage.ToBitmap().HorizontalResolution, currentImage.ToBitmap().VerticalResolution);


                    using (Graphics graphics = Graphics.FromImage(newImage))
                    {
                        graphics.CompositingQuality = CompositingQuality.HighQuality;
                        graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                        graphics.SmoothingMode = SmoothingMode.HighQuality;
                        graphics.DrawImage(currentImage.ToBitmap(), 0, 0, newWidth, newHeight);
                        graphics.Dispose();
                    }
                    //newImage.Dispose();

                   

                    //uploadimage = resizeimage(uploadimage);  

                    //currentImage = currentImage.Resize(newWidth, newHeight, Emgu.CV.CvEnum.INTER.CV_INTER_AREA,false);
                    //resizedImage = currentImage.Clone();

                    uploadsideview.Image = new Image<Bgr, Byte>(newImage);
                    resizedImage = new Image<Bgr,Byte>(newImage).Clone();
                    // uploadsideview.Image = currentImage;

                    var originalHeight = 450 / 2;
                    var newHeightcopy = originalHeight - (uploadsideview.Image.Size.Height / 2);
                    uploadsideview.Location = new Point(2, newHeightcopy);
                    

                    uploadsideview.Height = uploadsideview.Image.Size.Height + 1;
                    rect.Height = uploadsideview.Image.Size.Height;
                    rect.Width = uploadsideview.Image.Size.Width;
                   if (rect.Height < rect.Width)
                    {

                        // rect.Width = rect.Height / 2;
                        int aspect = rect.Width / rect.Height;
                        rect.Width = rect.Height / aspect;

                    }
                        
                    float f1 = uploadsideview.Width / 2;
                    float f2 = f1 - (rect.Width / 2);

                    float f3 = uploadsideview.Height / 2;
                    float f4 = f3 - (rect.Height / 2);

                    rect.X = (int)f2;
                    rect.Y = (int)f4 - 1;

                    isrectmoved = false;

                }
                catch(Exception ex)
                {
                  
                }
                
                open.Dispose();
            }
        }
        
        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {

            // Create pen.
            Pen redPen = new Pen(Color.Red, 1);
           
            e.Graphics.DrawRectangle(redPen, rect);
            redPen.Dispose();

        }

        private void uploadsideview_mousemove(object sender, MouseEventArgs e)
        {
            // MessageBox.Show("hi");
            if (isMouseDown == true)
            {
                isrectmoved = true;
                rect.Location = e.Location;
                // rect.Height = uploadsideview.Image == null ? 240 : uploadsideview.Height;

                // rect.Height = 242;
                rect.Height = uploadsideview.Height - 1;
                if (rect.Right > uploadsideview.Width)
                {
                    rect.X = (uploadsideview.Width - rect.Width) - 1;

                }
                if (rect.Top < 0)
                {
                    rect.Y = 0;
                }
                if (rect.Left < 0)
                {
                    rect.X = 0;

                }
                if (rect.Bottom > uploadsideview.Height)
                {
                    rect.Y = 0;
                }
                Refresh();
            }

        }

        private void uploadsideview_mouseup(object sender, MouseEventArgs e)
        {
            isMouseDown = false;
        }

        private void uploadsideview_mousedown(object sender, MouseEventArgs e)
        {
            isMouseDown = true;

        }

        private void rotateimage_Click(object sender, EventArgs e)
        {
            //uploadsideview.Image.RotateFlip(RotateFlipType.Rotate90FlipNone);
            Image<Bgr, Byte> flipimage = (Image<Bgr, Byte>)uploadsideview.Image;
            flipimage = flipimage.Rotate(90, new Bgr(255, 255, 255), false);
           
          /*  if (flipimage == null)
                flipimage = resizedImage.Rotate(90, new Bgr(255, 255, 255), false);
            else
                flipimage = flipimage.Rotate(90, new Bgr(255, 255, 255), false);*/
          
            uploadsideview.Image = flipimage;
        }

        private void openresultbutton_MouseDown(object sender, MouseEventArgs e)
        {
            disposeMemory(openresultbutton.Image);
            openresultbutton.Image = Yugamiru.Properties.Resources.dataload_down;
        }

        private void openresultbutton_MouseUp(object sender, MouseEventArgs e)
        {
            disposeMemory(openresultbutton.Image);
            openresultbutton.Image = Yugamiru.Properties.Resources.dataload_on;

        }

       

        /// <summary>
        /// To reset all the controls with the default values
        /// </summary>
        /// <remarks>
        /// While updating the language culture with a selected language, 
        /// this method needs to be called to reset all the control images, so the images will be updated 
        /// with the selected language images
        /// </remarks>
        private void SetControls()
        {
            //Default Labels
            status = "side";
            dragiconstatus = "belt";
            labelforiconsscreen.Text = "Drag icons to belt, ankles.";
            labelforsideicon.Text = "Drag each icons in side view.";

            //Reset dropdown selected values
            Gender.SelectedIndex = 0;
            Month.SelectedIndex = 0;
            Day.SelectedIndex = 0;

            disposeMemory(mainpage.Image);
            disposeMemory(startinitial.Image);
            disposeMemory(openresultbutton.Image);
            disposeMemory(quitapp.Image);

            disposeMemory(minimumbeltankles.Image);
            disposeMemory(maximumbeltankles.Image);
            disposeMemory(originalsizebeltankles.Image);

            disposeMemory(minimizesideview.Image);
            disposeMemory(maximizesideview.Image);
            disposeMemory(originalsizesideview.Image);

            disposeMemory(minimizefinalscreen.Image);
            disposeMemory(maximizefinalscreen.Image);
            disposeMemory(originalsizefinalscreen.Image);


            //Default control images
            mainpage.Image = Yugamiru.Properties.Resources.Mainpic;
            startinitial.Image = Yugamiru.Properties.Resources.startgreen_on;
            openresultbutton.Image = Yugamiru.Properties.Resources.dataload_up;
            quitapp.Image = Yugamiru.Properties.Resources.end_up;
          

            minimumbeltankles.Image = Yugamiru.Properties.Resources.mag1_down;
            maximumbeltankles.Image = Yugamiru.Properties.Resources.mag2_down;
            originalsizebeltankles.Image = Yugamiru.Properties.Resources.mag3_down;

            minimizesideview.Image = Yugamiru.Properties.Resources.mag1_down;
            maximizesideview.Image = Yugamiru.Properties.Resources.mag2_down;
            originalsizesideview.Image = Yugamiru.Properties.Resources.mag3_down;

            minimizefinalscreen.Image = Yugamiru.Properties.Resources.mag1_down;
            maximizefinalscreen.Image = Yugamiru.Properties.Resources.mag2_down;
            originalsizefinalscreen.Image = Yugamiru.Properties.Resources.mag3_down;
        }

        private void vScrollBar1_Scroll(object sender, ScrollEventArgs e)
        {
            var selectedValue = e.NewValue / 2;
            imageBox1.Image = imagePyramid_upright[selectedValue];
            imageBox2.Image = imagepyramid_crouched[selectedValue];

            //circle1 = new Rectangle(150, 300, (10 * selectedValue), (10 * selectedValue));

        } 

        private void quitapp_MouseDown(object sender, MouseEventArgs e)
        {
            disposeMemory(quitapp.Image);
            quitapp.Image = Yugamiru.Properties.Resources.end_down;
        }

        private void quitapp_MouseUp(object sender, MouseEventArgs e)
        {
            disposeMemory(quitapp.Image);
            quitapp.Image = Yugamiru.Properties.Resources.end_on;
        }

        private void returnbutton_MouseDown(object sender, MouseEventArgs e)
        {
            disposeMemory(returnbutton.Image);
            returnbutton.Image = Yugamiru.Properties.Resources.gobackgreen_down;
        }

        private void returnbutton_MouseUp(object sender, MouseEventArgs e)
        {
            disposeMemory(returnbutton.Image);
            returnbutton.Image = Yugamiru.Properties.Resources.gobackgreen_up;
        }

        private void nextbutton_MouseDown(object sender, MouseEventArgs e)
        {
            disposeMemory(nextbutton.Image);
            nextbutton.Image = Yugamiru.Properties.Resources.gonextgreen_down;

        }

        private void nextbutton_MouseUp(object sender, MouseEventArgs e)
        {
            disposeMemory(nextbutton.Image);
            nextbutton.Image = Yugamiru.Properties.Resources.gonextgreen_down;
        }

        private void enteruprightimage_MouseDown(object sender, MouseEventArgs e)
        {
            disposeMemory(enteruprightimage.Image);
            enteruprightimage.Image = Yugamiru.Properties.Resources.imagecopy_down;
        }

        private void enteruprightimage_MouseUp(object sender, MouseEventArgs e)
        {
            disposeMemory(enteruprightimage.Image);
            enteruprightimage.Image = Yugamiru.Properties.Resources.imagecopy_up;
        }

        private void openimage_MouseDown(object sender, MouseEventArgs e)
        {
            disposeMemory(openimage.Image);
            openimage.Image = Yugamiru.Properties.Resources.imageload_down;
        }

        private void openimage_MouseUp(object sender, MouseEventArgs e)
        {
            disposeMemory(openimage.Image);
            openimage.Image = Yugamiru.Properties.Resources.imageload_up;
        }

        private void rotateimage_MouseDown(object sender, MouseEventArgs e)
        {
            disposeMemory(rotateimage.Image);
            rotateimage.Image = Yugamiru.Properties.Resources.imagerotation_down;
        }

        private void rotateimage_MouseUp(object sender, MouseEventArgs e)
        {
            disposeMemory(rotateimage.Image);
            rotateimage.Image = Yugamiru.Properties.Resources.imagerotation_up;
        }

        private void editid_MouseDown(object sender, MouseEventArgs e)
        {
            disposeMemory(editid.Image);
            editid.Image = Yugamiru.Properties.Resources.namechange_down;
        }

        private void editid_MouseUp(object sender, MouseEventArgs e)
        {
            disposeMemory(editid.Image);
            editid.Image = Yugamiru.Properties.Resources.namechange_up;
        }

        private void Finish_MouseDown(object sender, MouseEventArgs e)
        {
            disposeMemory(Finish.Image);
            Finish.Image = Yugamiru.Properties.Resources.completegreen_down;
        }

        private void Finish_MouseUp(object sender, MouseEventArgs e)
        {
            disposeMemory(Finish.Image);
            Finish.Image = Yugamiru.Properties.Resources.completegreen_up;
        }

        private void changebutton_MouseDown(object sender, MouseEventArgs e)
        {
            disposeMemory(changebutton.Image);
            changebutton.Image = Yugamiru.Properties.Resources.imagechange_down;
        }

        private void changebutton_MouseUp(object sender, MouseEventArgs e)
        {
            disposeMemory(changebutton.Image);
            changebutton.Image = Yugamiru.Properties.Resources.imagechange_on;
        }

        private void checkpositionbutton_MouseDown(object sender, MouseEventArgs e)
        {
            disposeMemory(checkpositionbutton.Image);
            checkpositionbutton.Image = Yugamiru.Properties.Resources.jointedit_down;
        }

        private void checkpositionbutton_MouseUp(object sender, MouseEventArgs e)
        {
            disposeMemory(checkpositionbutton.Image);
            checkpositionbutton.Image = Yugamiru.Properties.Resources.jointedit_on;
        }

        private void showreportbutton_MouseDown(object sender, MouseEventArgs e)
        {
            disposeMemory(showreportbutton.Image);
            showreportbutton.Image = Yugamiru.Properties.Resources.reportdisplay_down;
        }

        private void showreportbutton_MouseUp(object sender, MouseEventArgs e)
        {
            disposeMemory(showreportbutton.Image);
            showreportbutton.Image = Yugamiru.Properties.Resources.reportdisplay_on;
        }

        private void printreportbutton_MouseDown(object sender, MouseEventArgs e)
        {
            disposeMemory(printreportbutton.Image);
            printreportbutton.Image = Yugamiru.Properties.Resources.reportprint_down;
        }

        private void printreportbutton_MouseUp(object sender, MouseEventArgs e)
        {
            disposeMemory(printreportbutton.Image);
            printreportbutton.Image = Yugamiru.Properties.Resources.reportprint_on;
        }

        private void saveresultbutton_MouseDown(object sender, MouseEventArgs e)
        {
            disposeMemory(saveresultbutton.Image);
            saveresultbutton.Image = Yugamiru.Properties.Resources.datasave_down;
        }

        private void saveresultbutton_MouseUp(object sender, MouseEventArgs e)
        {
            disposeMemory(saveresultbutton.Image);
            saveresultbutton.Image = Yugamiru.Properties.Resources.datasave_on;
        }

        private void restartbutton_MouseDown(object sender, MouseEventArgs e)
        {
            disposeMemory(restartbutton.Image);
            restartbutton.Image = Yugamiru.Properties.Resources.startred_down;
        }

        private void restartbutton_MouseUp(object sender, MouseEventArgs e)
        {
            disposeMemory(restartbutton.Image);
            restartbutton.Image = Yugamiru.Properties.Resources.startred_on;
        }

        private void initialscreenbutton_MouseDown(object sender, MouseEventArgs e)
        {
            disposeMemory(initialscreenbutton.Image);
            initialscreenbutton.Image = Yugamiru.Properties.Resources.returnstart_down;
        }

        private void initialscreenbutton_MouseUp(object sender, MouseEventArgs e)
        {
            disposeMemory(initialscreenbutton.Image);
            initialscreenbutton.Image = Yugamiru.Properties.Resources.returnstart_on;
        }
        private void imageBox1_Paint(object sender, PaintEventArgs e)
        {
            switch (dragiconstatus)
            {
                case "belt":
                    e.Graphics.FillEllipse(Brushes.Red, circle1);
                    e.Graphics.FillEllipse(Brushes.Red, circle2);
                    e.Graphics.FillRectangle(Brushes.Red, rect1);
                    e.Graphics.FillRectangle(Brushes.Red, rect2);
                    break;
                case "knees":
                    e.Graphics.FillEllipse(Brushes.Green, circle5);
                    e.Graphics.FillEllipse(Brushes.Green, circle6);
                    Pen yellowpen = new Pen(Brushes.Yellow, 3);
                    e.Graphics.DrawLine(yellowpen,202,254,232,254);

                    e.Graphics.FillEllipse(Brushes.Red, circle7);
                    e.Graphics.FillEllipse(Brushes.Red, circle8);
                    e.Graphics.DrawLine(yellowpen, 202, 254, 192, 298);
                    e.Graphics.DrawLine(yellowpen, 232, 254, 240, 300);

                    e.Graphics.FillEllipse(Brushes.Green, circle9);
                    e.Graphics.FillEllipse(Brushes.Green, circle10);
                    e.Graphics.DrawLine(yellowpen,192,298,192,400);
                    e.Graphics.DrawLine(yellowpen,240,300,242,400);
                    yellowpen.Dispose();
                    break;
                case "shoulder":
                    break;
                default:
                    break;

            }

        }
        private void imageBox1_MouseDown(object sender, MouseEventArgs e)
        {
               if ((e.X >= circle1.X) && (e.X <= (circle1.X + circle1.Width)) && (e.Y >= circle1.Y) && (e.Y <= (circle1.Y + circle1.Height)))
               {
                   firstCircle = true;
               }
               else if ((e.X >= circle2.X) && (e.X <= (circle2.X + circle2.Width)) &&(e.Y >= circle2.Y) && (e.Y <= (circle2.Y + circle2.Height)))
               {
                   secondCircle = true;
               }
               else if ((e.X >= rect1.X) && (e.X <= (rect1.X + rect1.Width)) && (e.Y >= rect1.Y)&& (e.Y <= (rect1.Y + rect1.Height)))
               {
                   firstrect = true;
               }
               else if ((e.X >= rect2.X) && (e.X <= (rect2.X + rect2.Width)) && (e.Y >= rect2.Y) && (e.Y <= (rect2.Y + rect2.Height)))
               {
                   secondrect = true;
               }
               else if((e.X >= circle5.X) && (e.X <= (circle5.X + circle5.Width)) && (e.Y >= circle5.Y) && (e.Y <= (circle5.Y + circle5.Height)))
                {
                kneesCircle = true;
                }
               mouseCliked = true;
          
        }

        private void imageBox1_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseCliked)
            {
                if (firstCircle)
                {
                    circle1.X = e.X;
                    circle1.Y = e.Y;
                }
                else if (secondCircle)
                {
                    circle2.X = e.X;
                    circle2.Y = e.Y;
                }
                else if (firstrect)
                {
                    rect1.X = e.X;
                    rect1.Y = e.Y;
                }
                else if(secondrect)
                {
                    rect2.X = e.X;
                    rect2.Y = e.Y;
                }
                else if(kneesCircle)
                {
                    circle5.X = e.X;
                    circle5.Y = e.Y;
                }
                Refresh();
            }

        }


        private void imageBox1_MouseUp(object sender, MouseEventArgs e)
        {
            firstCircle = false;
            secondCircle = false;
            firstrect = false;
            secondrect = false;
            mouseCliked = false;
            kneesCircle = false;
        }

        private void imageBox1_MouseLeave(object sender, EventArgs e)
        {
            firstCircle = false;
            secondCircle = false;
            firstrect = false;
            secondrect = false;
            mouseCliked = false;
            kneesCircle = false;

        }

        private void vScrollBarsideview_Scroll(object sender, ScrollEventArgs e)
        {
            var selectedValue = e.NewValue / 2;
            imageBox3.Image = imagepyramid_side[selectedValue];       
        }
        private void disposeMemory(Image objImage)
        {
            if(objImage != null)
            {
                objImage.Dispose();
                objImage = null;
            }

        }

        private void imageBox2_MouseDown(object sender, MouseEventArgs e)
        {
            if ((e.X >= circle3.X) && (e.X <= (circle3.X + circle3.Width)) && (e.Y >= circle3.Y) && (e.Y <= (circle3.Y + circle3.Height)))
            {
                firstCircle = true;
            }
            else if ((e.X >= circle4.X) && (e.X <= (circle4.X + circle4.Width)) && (e.Y >= circle4.Y) && (e.Y <= (circle4.Y + circle4.Height)))
            {
                secondCircle = true;
            }
            else if ((e.X >= rect3.X) && (e.X <= (rect3.X + rect3.Width)) && (e.Y >= rect3.Y) && (e.Y <= (rect3.Y + rect3.Height)))
            {
                firstrect = true;
            }
            else if ((e.X >= rect4.X) && (e.X <= (rect4.X + rect4.Width)) && (e.Y >= rect4.Y) && (e.Y <= (rect4.Y + rect4.Height)))
            {
                secondrect = true;
            }
            mouseCliked = true;

        }

        private void imageBox2_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseCliked)
            {
                if (firstCircle)
                {
                    circle3.X = e.X;
                    circle3.Y = e.Y;
                }
                else if (secondCircle)
                {
                    circle4.X = e.X;
                    circle4.Y = e.Y;
                }
                else if (firstrect)
                {
                    rect3.X = e.X;
                    rect3.Y = e.Y;
                }
                else if (secondrect)
                {
                    rect4.X = e.X;
                    rect4.Y = e.Y;
                }
                Refresh();
            }

        }

        private void imageBox2_MouseUp(object sender, MouseEventArgs e)
        {
            firstCircle = false;
            secondCircle = false;
            firstrect = false;
            secondrect = false;
            mouseCliked = false;

        }

        private void imageBox2_MouseLeave(object sender, EventArgs e)
        {
            firstCircle = false;
            secondCircle = false;
            firstrect = false;
            secondrect = false;
            mouseCliked = false;

        }

        private void imageBox2_Paint(object sender, PaintEventArgs e)
        {
            switch(dragiconstatus)
            { 
            case "belt":
                e.Graphics.FillEllipse(Brushes.Red, circle3);
                e.Graphics.FillEllipse(Brushes.Red, circle4);
                e.Graphics.FillRectangle(Brushes.Red, rect3);
                e.Graphics.FillRectangle(Brushes.Red, rect4);
                break;

            case "knees":
                e.Graphics.FillEllipse(Brushes.Green, circle11);
                e.Graphics.FillEllipse(Brushes.Green, circle12);
                Pen yellowpen = new Pen(Brushes.Yellow, 3);
                e.Graphics.DrawLine(yellowpen, 202, 254, 232, 254);

                e.Graphics.FillEllipse(Brushes.Red, circle13);
                e.Graphics.FillEllipse(Brushes.Red, circle14);
                e.Graphics.DrawLine(yellowpen, 202, 254, 192, 298);
                e.Graphics.DrawLine(yellowpen, 232, 254, 240, 300);

                e.Graphics.FillEllipse(Brushes.Green, circle15);
                e.Graphics.FillEllipse(Brushes.Green, circle16);
                e.Graphics.DrawLine(yellowpen, 192, 298, 192, 400);
                e.Graphics.DrawLine(yellowpen, 240, 300, 242, 400);
                yellowpen.Dispose();
                break;

                case "shoulder":
                break;

                default:
                break;
            }

        }
    }
}
