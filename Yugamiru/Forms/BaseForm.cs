﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Yugamiru.Forms
{
    public partial class BaseForm : Form
    {
        public BaseForm()
        {
            InitializeComponent();
        }

        private void BaseForm_Load(object sender, EventArgs e)
        {

        }

        private void hELPToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void versionInfoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Version Info");
            
        }

        private void splitter1_SplitterMoved(object sender, SplitterEventArgs e)
        {

        }

        private void versionInfoToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("VersionInfo");
        }

        private void pORTSettingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PORT portui = new PORT();
            portui.Show();

        }

    }
}
